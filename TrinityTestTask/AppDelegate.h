//
//  AppDelegate.h
//  TrinityTestTask
//
//  Created by John FrostFox on 26/07/16.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

