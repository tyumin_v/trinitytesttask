//
//  DataDownloader.h
//  TrinityTestTask
//
//  Created by John FrostFox on 27/07/16.
//
//

#import <Foundation/Foundation.h>

@interface DataDownloader : NSObject

//- (NSData *)downloadData:(NSURL *)url;

- (void)retrieveData:(NSString *)path completionHandler:(void (^)(NSData * downloadedData))completionHandler;

@end
