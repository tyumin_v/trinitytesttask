//
//  DataDownloader.m
//  TrinityTestTask
//
//  Created by John FrostFox on 27/07/16.
//
//

#import "DataDownloader.h"

@interface DataDownloader ()

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSURL *baseUrl;

@end

@implementation DataDownloader

- (instancetype)init {
    self = [super init];
    if (self) {
        _session = [NSURLSession sharedSession];
        _baseUrl = [NSURL URLWithString:@"http://balasdevserver.trinitydigital.ru/"];
    }
    return self;
}


- (void)retrieveData:(NSString *)path completionHandler:(void (^)(NSData * downloadedData))completionHandler;
{
    NSURL * url = [NSURL URLWithString:path relativeToURL:self.baseUrl];
    
    NSURLSessionDataTask * dataTask = [self.session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error != nil) {
            // If any error occurs then just display its description on the console.
            NSLog(@"%@", [error localizedDescription]);
        }
        else{
            // If no error occurs, check the HTTP status code.
            NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
            
            // If it's other than 200, then show it on the console.
            if (HTTPStatusCode != 200) {
                NSLog(@"HTTP status code = %ld", (long)HTTPStatusCode);
            }
            
            if (completionHandler) {
                completionHandler(data);
            }
        }
    }];
    [dataTask resume];
}

@end
