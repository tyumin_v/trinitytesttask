//
//  QuestionAnswerCellStructure.h
//  TrinityTestTask
//
//  Created by John FrostFox on 28/07/16.
//
//

#import <Foundation/Foundation.h>

@interface QuestionAnswerCellStructure : NSObject

@property (strong) NSString *contents;
@property (nonatomic, getter=isChosen) BOOL chosen;
//@property (nonatomic, getter=isMatched) BOOL matched;
@property (nonatomic, strong) NSMutableArray *links;

@end
