//
//  QuestionAnswerCellStructure.m
//  TrinityTestTask
//
//  Created by John FrostFox on 28/07/16.
//
//

#import "QuestionAnswerCellStructure.h"

@implementation QuestionAnswerCellStructure

@synthesize chosen = _chosen;
//@synthesize matched = _matched;

- (instancetype)init {
    self = [super init];

    if (self) {
        _links = [NSMutableArray array];
        _chosen = NO;
        _contents = @"";
    }
    
    return self;
}

@end
