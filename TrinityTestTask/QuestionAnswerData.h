//
//  QuestionAnswerModel.h
//  TrinityTestTask
//
//  Created by John FrostFox on 28/07/16.
//
//

#import <Foundation/Foundation.h>

typedef void(^DataLoaded)(BOOL isLoaded);

@interface QuestionAnswerData : NSObject


@property (nonatomic, strong, readonly) NSArray *questions;
@property (nonatomic, strong, readonly) NSArray *answers;

//+ (instancetype)sharedQAData;

- (void)loadData:(DataLoaded)callbackBlock;


@end
