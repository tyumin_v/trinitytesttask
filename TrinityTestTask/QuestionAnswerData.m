//
//  QuestionAnswerModel.m
//  TrinityTestTask
//
//  Created by John FrostFox on 28/07/16.
//
//

#import "QuestionAnswerData.h"
#import "DataDownloader.h"

@interface QuestionAnswerData ()

@property (nonatomic, strong) NSMutableArray *innerQuestions;
@property (nonatomic, strong) NSMutableArray *innerAnswers;

@property (nonatomic, strong) DataDownloader * api;

@end

@implementation QuestionAnswerData

//+ (instancetype)sharedQAData{
//    static QuestionAnswerData *model = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        model = [[[self class] alloc] init];
//    });
//    return model;
//}

-(instancetype)init {
    self = [super init];
    if (self) {
        _innerQuestions = [NSMutableArray array];
        _innerAnswers = [NSMutableArray array];
        _api = [[DataDownloader alloc] init];
    }
    return self;
}

#pragma mark - getters
- (NSArray *)questions {
    NSArray *returnQuestions = [self.innerQuestions copy];
    return returnQuestions;
}

- (NSArray *)answers {
    NSArray *returnAnswers = [self.innerAnswers copy];
    return returnAnswers;
}

#pragma mark - loader
/// загрузка
- (void)loadData:(void (^)(BOOL isLoaded))callbackBlock; {
    __weak typeof(self) weakSelf = self;
    NSLog(@"Model - LoadData started");
    
    [self.api retrieveData:@"/data"
                 completionHandler:^(NSData *downloadedData) {
                     NSLog(@"Model - LoadData data arrived");
                     [weakSelf processData:downloadedData withCallback:callbackBlock];
                 }];
}

#pragma mark - parser
/// парсинг
- (void)processData:(NSData*)data withCallback:(DataLoaded)callback {
    
    
    
    if(NSClassFromString(@"NSJSONSerialization"))
    {
        NSError *error = nil;
        id object = [NSJSONSerialization
                     JSONObjectWithData:data
                     options:0
                     error:&error];
        
        if(error) {
            /* JSON was malformed, act appropriately here */
            callback(NO);
            return;
        }

        // the originating poster wants to deal with dictionaries;
        // assuming you do too then something like this is the first
        // validation step:
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *results = object;
//            NSArray *array = [results objectForKey:@"test"];
            NSArray *questions = [[results objectForKey:@"test"] objectForKey:@"left"];
            NSArray *answers = [[results objectForKey:@"test"] objectForKey:@"right"];
            [questions enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
                [_innerQuestions addObject:obj];
            }];
            [answers enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
                [_innerAnswers addObject:obj];
            }];
//            NSLog(@"вопросы : %@", _innerQuestions);
//            NSLog(@"ответы : %@", _innerAnswers);
            callback(YES);
            return;
            /* proceed with results as you like; the assignment to
             an explicit NSDictionary * is artificial step to get
             compile-time checking from here on down (and better autocompletion
             when editing). You could have just made object an NSDictionary *
             in the first place but stylistically you might prefer to keep
             the question of type open until it's confirmed */
        }
        else
        {
            callback(NO);
            return;
            /* there's no guarantee that the outermost object in a JSON
             packet will be a dictionary; if we get here then it wasn't,
             so 'object' shouldn't be treated as an NSDictionary; probably
             you need to report a suitable error condition */
        }
    }
    else
    {
        callback(NO);
        return;
        // the user is using iOS 4; we'll need to use a third-party solution.
        // If you don't intend to support iOS 4 then get rid of this entire
        // conditional and just jump straight to
        // NSError *error = nil;
        // [NSJSONSerialization JSONObjectWithData:...
    }
    

    
}


@end
