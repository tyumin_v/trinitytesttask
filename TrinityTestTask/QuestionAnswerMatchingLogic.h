//
//  QuestionAnswerMatchingModel.h
//  TrinityTestTask
//
//  Created by John FrostFox on 28/07/16.
//
//

#import <Foundation/Foundation.h>
#import "QuestionAnswerCellStructure.h"

@interface QuestionAnswerMatchingLogic : NSObject

- (void)addQuestions:(NSArray *)questionObjects;
- (void)addAnswers:(NSArray *)answerObjects;

- (void)chooseQuestionAtIndex:(NSUInteger)index;
- (void)chooseAnswerAtIndex:(NSUInteger)index;

- (NSInteger)numberOfQuestions;
- (NSInteger)numberOfAnswers;

- (QuestionAnswerCellStructure *)questionAtIndex:(NSUInteger)index;
- (QuestionAnswerCellStructure *)answerAtIndex:(NSUInteger)index;

//- (void)matchQuestionsWithAnswer:(NSMutableArray *)chosenQuestions chosenAnswer:(NSUInteger *)chosenAnswer;
//- (void)matchAnswersWithQuestion:(NSMutableArray *)chosenAnswers chosenQueston:(NSUInteger *)chosenQuestion;


@end
