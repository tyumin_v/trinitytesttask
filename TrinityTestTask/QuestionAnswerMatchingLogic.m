//
//  QuestionAnswerMatchingModel.m
//  TrinityTestTask
//
//  Created by John FrostFox on 28/07/16.
//
//

#import "QuestionAnswerMatchingLogic.h"

@interface QuestionAnswerMatchingLogic ()

@property (nonatomic, strong) NSMutableArray *questions; //of QuestionAnswerCellStructure
@property (nonatomic, strong) NSMutableArray *answers; //of QuestionAnswerCellStructure

@property (nonatomic, strong) QuestionAnswerCellStructure *currentQuestion;

//@property (nonatomic) BOOL matchByAnswers;
//@property (nonatomic) BOOL matchByQuestions;
//@property (nonatomic, strong) NSMutableArray *links; //of NSMutableArray of NSUInteger
                                                     //index is an element for questions, content refers to answers
//@property (nonatomic, strong) NSMutableArray *linksContent;

@end

@implementation QuestionAnswerMatchingLogic

@synthesize questions = _questions;
@synthesize answers = _answers;

- (instancetype)init {
    self = [super init];
    if (self){
        self.questions = [NSMutableArray array];
        self.answers = [NSMutableArray array];
        //self.links = [NSMutableArray array];
    }
    return self;
}

- (void)addQuestions:(NSArray *)questionStrings{
    for (NSString *question in questionStrings) {
        QuestionAnswerCellStructure * q = [QuestionAnswerCellStructure new];
        q.contents = question;
        [self.questions addObject: q];
    }
}

- (void)addAnswers:(NSArray *)answerStrings{
    for (NSString *answer in answerStrings) {
        QuestionAnswerCellStructure * a = [QuestionAnswerCellStructure new];
        a.contents = answer;
        [self.answers addObject:a];
    }
}



- (void)chooseQuestionAtIndex:(NSUInteger)index{
    QuestionAnswerCellStructure* questionObject = [self questionAtIndex:index];
    if (questionObject.isChosen){
        questionObject.chosen = NO;
    }
    else {
        for (QuestionAnswerCellStructure *questionObj in self.questions) {
            if (questionObj.isChosen) {
                questionObj.chosen = NO;
            }
        }
        questionObject.chosen = YES;
    }
}



- (NSInteger)numberOfQuestions
{
    return [_questions count];
}

- (NSInteger)numberOfAnswers
{
    return [_answers count];
}


- (void)chooseAnswerAtIndex:(NSUInteger)index{
//    QuestionAnswerCellStructure* answerObject = [self answerAtIndex:index];
    for (QuestionAnswerCellStructure *questionObject in self.questions) {
        if (questionObject.isChosen) {
            if ([questionObject.links firstObjectCommonWithArray:@[@(index)]]) {
                [questionObject.links removeObjectsInArray:@[@(index)]];
            } else {
                [questionObject.links addObject:@(index)];
            }
        }
    }

}

- (QuestionAnswerCellStructure *)questionAtIndex:(NSUInteger)index{
    return ([[self questions] count] > index) ? self.questions[index] : nil;
}

- (QuestionAnswerCellStructure *)answerAtIndex:(NSUInteger)index{
    return ([[self answers] count] > index) ? self.answers[index] : nil;
}

@end
