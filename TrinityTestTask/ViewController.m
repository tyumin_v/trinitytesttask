//
//  ViewController.m
//  TrinityTestTask
//
//  Created by John FrostFox on 26/07/16.
//
//

#import "ViewController.h"
#import "QuestionAnswerMatchingLogic.h"
#import "QuestionAnswerData.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) QuestionAnswerMatchingLogic* app;
@property (nonatomic, strong) QuestionAnswerData* qAData;
@property (nonatomic, strong) NSMutableArray* cellColors; //of UIColor;

//@property (nonatomic) BOOL loaded;

@property (weak, nonatomic) IBOutlet UITableView *leftTable;
@property (weak, nonatomic) IBOutlet UITableView *rightTable;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
//@property (weak, nonatomic) IBOutlet UIActivityIndicatorView * loadingIndicatorView;

@end

@implementation ViewController

//- (QuestionAnswerMatchingLogic *)app{
//    if (!_app) {
//        _app = [[QuestionAnswerMatchingLogic alloc] init];
//    }
//    return _app;
//}
//
//- (QuestionAnswerData *)qAData{
//    if (!_qAData) {
//        _qAData = [[QuestionAnswerData alloc] init];
//    }
//    return _qAData;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    if (!_qAData) {
        _qAData = [[QuestionAnswerData alloc] init];
    }
    if (!_app) {
        _app = [[QuestionAnswerMatchingLogic alloc] init];
    }
    if (!_cellColors) {
        _cellColors = [NSMutableArray array];
    }
    
    [self showLoading];
    // Do any additional setup after loading the view, typically from a nib.
    
    [_qAData loadData:^(BOOL isLoaded) {
        if (isLoaded) {
            NSLog(@"данные загрузились");
            dispatch_async(dispatch_get_main_queue(), ^{
                //[_loadingView setHidden:YES];
                [weakSelf reloadTables];
                [_cellColors removeAllObjects];
                for (NSInteger i = 0; i < [_app numberOfQuestions]; i++) {
                    
                    //Distributed under The MIT License:
                    //http://opensource.org/licenses/mit-license.php
                    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
                    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
                    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
                    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
                    //end
                    
                    [_cellColors addObject:color];
                }
            });
        }
    }];
//    [_app addQuestions:[_qAData questions]];
//    [_app addAnswers:[_qAData answers]];
//    [self reloadTables];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadTables{
    
//    NSLog(@"загрузка вопросов");
    [_app addQuestions:[_qAData questions]];
    [_leftTable reloadData];
    
//    NSLog(@"загруза ответов");
    [_app addAnswers:[_qAData answers]];
    [_rightTable reloadData];

    [self hideLoading];

}

- (void)showLoading {
    [self.view sendSubviewToBack:_loadingView];
    [_loadingView setHidden:NO];
//    [_loadingIndicatorView setHidden:NO];
//    [_loadingIndicatorView startAnimating];
}

- (void)hideLoading {
    [self.view bringSubviewToFront:_loadingView];
    [_loadingView setHidden:YES];
//    [_loadingIndicatorView stopAnimating];
}
//сколько ячеек
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.leftTable) {
//        NSLog(@"левая таблица %ld", (long)[_app numberOfQuestions]);
        return [_app numberOfQuestions];

    }
    if (tableView == self.rightTable) {
//        NSLog(@"правая таблица %ld", (long)[_app numberOfAnswers]);
        return [_app numberOfAnswers];
    }
    return 0;
}

//что в каждой ячейке отображать
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *leftTableIdentifier = @"leftTableCell";
    static NSString *rightTableIdentifier = @"rightTableCell";
    
    BOOL isQuestion = (tableView == self.leftTable);
    
    UITableViewCell *cell; [tableView dequeueReusableCellWithIdentifier: isQuestion ? leftTableIdentifier : rightTableIdentifier ];

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:isQuestion ? leftTableIdentifier : rightTableIdentifier];
    }
    
    if (tableView == self.leftTable){
//        NSLog(@"загружаю в левую ячейку");
        cell.textLabel.text = [[_app questionAtIndex:indexPath.row] contents];
//        NSLog(@"загрузил в левую ячейку");
    }
    if (tableView == self.rightTable){
//        NSLog(@"загружаю в правую ячейку");
        cell.textLabel.text = [[_app answerAtIndex:indexPath.row]contents];
//        NSLog(@"загрузил в правую ячейку");
    }
    return cell;
}


#pragma mark - TableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BOOL isQuestion = (tableView == self.leftTable);
    QuestionAnswerCellStructure * data;
    if(isQuestion) {
        [_app chooseQuestionAtIndex:indexPath.row];
        data = [_app questionAtIndex:indexPath.row];
        for (UITableViewCell *questionCell in [tableView visibleCells]) {
            [questionCell setBackgroundColor:[UIColor clearColor]];
        }
        for (UITableViewCell *answerCell in [self.rightTable visibleCells]) {
            [answerCell setBackgroundColor:[UIColor clearColor]];
        }
        if(data.isChosen) {
            [cell setBackgroundColor:[_cellColors objectAtIndex:indexPath.row]];
            for (NSNumber *linkedAnswerIndex in [data links]) {
                NSIndexPath *answerIndexPath = [NSIndexPath indexPathForRow:[linkedAnswerIndex intValue] inSection:0];
                [[self.rightTable cellForRowAtIndexPath:answerIndexPath] setBackgroundColor:[_cellColors objectAtIndex:indexPath.row]];
            }
        } else {
            if (data && data.links && data.links.count) {
                //[cell setAlpha:0.5];
                for (NSNumber *linkedAnswerIndex in [data links]) {
                    NSIndexPath *answerIndexPath = [NSIndexPath indexPathForRow:[linkedAnswerIndex intValue] inSection:0];
                    [[self.rightTable cellForRowAtIndexPath:answerIndexPath] setBackgroundColor:[UIColor clearColor]];
                }
            } else {
                [cell setBackgroundColor:[UIColor clearColor]];
            }
        }
    } else {
        [_app chooseAnswerAtIndex:indexPath.row];
        data = [_app answerAtIndex:indexPath.row];
        for (int i = 0; i < [_app numberOfQuestions]; i++) {
            if ([[_app questionAtIndex:i] isChosen]) {
                [cell setBackgroundColor:[_cellColors objectAtIndex:i]];
                break;
            }
            [cell setBackgroundColor:[UIColor clearColor]];
        }
    }
}

@end
